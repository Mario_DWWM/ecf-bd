<?php

get_header();

while (have_posts()) :	the_post();

?>
	<div class="new-release-page">

		<div class="new-comic">

			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

			<?php the_post_thumbnail(); ?>
			<ul>
				<li><img src="<?php the_field('image'); ?>"></li>
				<li><img src="<?php the_field('image0'); ?>"></li>
				<li><img src="<?php the_field('image1'); ?>"></li>
				<li><img src="<?php the_field('image2'); ?>"></li>
				<li><img src="<?php the_field('image3'); ?>"></li>
			</ul>
			<?php the_content();

			global $post;
			foreach (get_the_tags($post->ID) as $tag) {
				echo '<li><a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a></li>';
			}
			?>

			<p>Written by:
				<?php the_author_link(); ?></p>
			<?php
			$categories = get_categories(array(
				'orderby' => 'name',
				'order'   => 'ASC'
			));

			foreach ($categories as $category) {
				$category_link = sprintf(
					'<a href="%1$s" alt="%2$s">%3$s</a>',
					esc_url(get_category_link($category->term_id)),
					esc_attr(sprintf(__('View all posts in %s', 'textdomain'), $category->name)),
					esc_html($category->name)
				);

				echo '<p>' . sprintf(esc_html__('Category: %s', 'textdomain'), $category_link) . '</p> ';
				// echo '<p>' . sprintf( esc_html__( 'Description: %s', 'textdomain' ), $category->description ) . '</p>';
			} ?>
		<?php

		if (is_attachment()) {

			// Parent post navigation.

			the_post_navigation(

				array(
					/* translators: %s: Parent post link. */
					'prev_text' => sprintf(__('<span class="meta-nav">Published in</span><span class="post-title">%s</span>', 'twentytwentyone'), '%title'),
				)
			);
		}

		// If comments are open or there is at least one comment, load up the comment template.

		if (comments_open() || get_comments_number()) {	comments_template();}

		// Previous/next post navigation.

		$twentytwentyone_next = is_rtl() ? twenty_twenty_one_get_icon_svg('ui', 'arrow_left') : twenty_twenty_one_get_icon_svg('ui', 'arrow_right');

		$twentytwentyone_prev = is_rtl() ? twenty_twenty_one_get_icon_svg('ui', 'arrow_right') : twenty_twenty_one_get_icon_svg('ui', 'arrow_left');

		$twentytwentyone_next_label     = esc_html__('Next post', 'twentytwentyone');

		$twentytwentyone_previous_label = esc_html__('Previous post', 'twentytwentyone');

		the_post_navigation(

			array(

				'next_text' => '<p class="meta-nav">' . $twentytwentyone_next_label . $twentytwentyone_next . '</p><p class="post-title">%title</p>',

				'prev_text' => '<p class="meta-nav">' . $twentytwentyone_prev . $twentytwentyone_previous_label . '</p><p class="post-title">%title</p>',
			)
		);
	endwhile; // End of the loop.
		?>
		</div>
	</div>
	<?php get_footer(); ?>