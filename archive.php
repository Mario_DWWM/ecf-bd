<?php /** Template Name: New Release */?>


<?php get_header(); ?>

<?php
$args = array('post_type'=> 'comic');

$the_query = new WP_Query($args);
?>



<div class="new-release-page">

	<?php if ( $the_query->have_posts() ) : ?>

		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<div class="new-comic">

				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

				<?php the_post_thumbnail(); ?>

				<img src="<?php the_field('image'); ?>" width="250">
				
				<?php the_excerpt();
				global $post;
			foreach (get_the_tags($post->ID) as $tag) {
				echo '<li><a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a></li>';
			}
			?>

			<p>Written by:
				<?php the_author_link(); ?></p>
			<?php
			$categories = get_categories(array(
				'orderby' => 'name',
				'order'   => 'ASC'
			));

			foreach ($categories as $category) {
				$category_link = sprintf(
					'<a href="%1$s" alt="%2$s">%3$s</a>',
					esc_url(get_category_link($category->term_id)),
					esc_attr(sprintf(__('View all posts in %s', 'textdomain'), $category->name)),
					esc_html($category->name)
				);

				echo '<p>' . sprintf(esc_html__('Category: %s', 'textdomain'), $category_link) . '</p> ';
			} ?>
			</div>

		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	<?php endif; ?>

</div>

<?php get_footer(); ?>